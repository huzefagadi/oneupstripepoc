package saiflimited.com.stripepoc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Account;
import com.stripe.model.Customer;
import com.stripe.net.RequestOptions;

import java.util.HashMap;
import java.util.Map;

import saiflimited.com.stripepoc.saiflimited.com.stripepoc.bean.User;
import saiflimited.com.stripepoc.saiflimited.com.stripepoc.utility.Constants;

public class MainActivity extends AppCompatActivity {

    Button signIn, createAccount;
    EditText username, password;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        editor = prefs.edit();
        String signedInUser = prefs.getString(Constants.SIGNED_IN_USER,null);
        if(signedInUser!=null)
        {
            Intent intent = new Intent(getApplicationContext(), ShoppingActivity.class);
            intent.putExtra("Customer", signedInUser);
            startActivity(intent);
            finish();
        }
        else
        {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Signing In");
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

            setSupportActionBar(toolbar);
            username = (EditText) findViewById(R.id.username);
            password = (EditText) findViewById(R.id.password);
            if (getIntent().getStringExtra("EmailId") != null) {
                username.setText(getIntent().getStringExtra("EmailId"));
            }

            signIn = (Button) findViewById(R.id.signIn);
            createAccount = (Button) findViewById(R.id.createAccount);
            createAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), CreateAccountActivity.class));
                    // finish();
                }
            });
            signIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String usernameText = username.getText().toString().trim();
                    String passwordText = password.getText().toString().trim();
                    if(usernameText.isEmpty() || passwordText.isEmpty() )
                    {
                        Toast.makeText(getApplicationContext(), "All fields are Mandatory!", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        if (prefs.getString(usernameText, null) != null) {
                            User user = new Gson().fromJson(prefs.getString(usernameText, null), User.class);
                            if(passwordText.equals(user.getPassword()))
                            {
                                progressDialog.show();
                                new CallWebService().execute(user.getCustomerId(),user.getAccountId());
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), "Incorrect Credentials", Toast.LENGTH_LONG).show();
                            }

                        } else {

                            Toast.makeText(getApplicationContext(), "User Doesn't exists", Toast.LENGTH_LONG).show();
                        }
                    }

                }
            });

        }

    }



    class CallWebService extends AsyncTask<String, Void, Customer> {

        String errorMessage = null;
        @Override
        protected Customer doInBackground(String... values) {

            Stripe.apiKey = Constants.PLATFORM_SECRET_KEY;
            try {

                RequestOptions requestOptions = RequestOptions.builder().setApiKey(values[1]).build();
                Customer customer = Customer.retrieve(values[0], requestOptions);

                return customer;
            } catch (AuthenticationException e) {
                e.printStackTrace();
                errorMessage = e.getMessage();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
                errorMessage = e.getMessage();
            } catch (APIConnectionException e) {
                e.printStackTrace();
                errorMessage = e.getMessage();
            } catch (CardException e) {
                e.printStackTrace();
                errorMessage = e.getMessage();
            } catch (APIException e) {
                e.printStackTrace();
                errorMessage = e.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final Customer account) {
            try {
                progressDialog.cancel();
            } catch (Exception e) {

            }
            if (account != null) {
                editor.putString(Constants.SIGNED_IN_USER,account.getDescription()).apply();
                Intent intent = new Intent(getApplicationContext(), ShoppingActivity.class);
                intent.putExtra("Customer", account.getDescription());
                startActivity(intent);
                finish();
            }
            else
            {
                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_LONG).show();
            }
        }
    }
}
